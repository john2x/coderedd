# About coderedd #
coderedd is a custom UI for reddit.com inspired by code editors. Makes browsing reddit look like you're coding! It is available in various programming languages (syntax) and with different color schemes to match your preferred development environment. 

Powered by [Django][]. Using the [reddit\_api][api]. Color schemes generated by
[Pygments][].

[Flask]: http://flask.pocoo.org
[api]: https://github.com/mellort/reddit_api
[Pygments]: http://pygments.org
[Django]: http://djangoproject.com

# Setup and Installation #

Install requirements.

	$ hg clone https://bitbucket.org/john2x/coderedd
	$ cd coderedd
    $ pip install -r requirements.txt
    
Run.

    $ python app.py

Open `http://localhost:8000/` in a browser.

# Adding a new syntax template #

Coderedd makes it easy to add new syntax options. The basic steps are:

1. Copy an existing syntax template (found in `templates/syntaxname/`) and make the necessary changes to it (make sure the links don't change and don't forget the elements' classes to give them syntax highlighting)
2. Add the syntax's name to the `syntaxes` setting in `settings.py`. 
3. ???
4. Profit!

There are several files in a syntax template folder. A short summary for each file and the available variables are listed.

## The Pages ##

These are the files which render the different types of reddit pages.

### index.html ###
Renders the front page.

- `next()` and `prev()` links in `div.nav`. 

### subreddit.html ###
Renders a particular subreddit.

- `subreddit` in `div.subreddit-info`
- `{{ subreddit }}` holds a subreddit's info
- `next()` and `prev()` links in `div.nav`. 

### comments.html ###
Renders a story's comments page.

You can modify how a comment is rendered here.

- `{{ comment }}` in the recursive `{% for %}` loop holds a comment's info

## The Included Snippets ##

These files are snippets which are included/inserted into the page templates above.

### header.html
The header. Goes on top. Includes navigation, authentication, list of subreddits.

- `{{ my_subreddits }}` holds a list of a user's subreddits.
- `{{ session.username }}` is the currently logged in user's username

### story.html
Renders a story's details (title, url, subreddit, poster, etc.)

- `{{ story }}` in the `{% for %}` loop holds a story's info

### footer.html
The footer.

Change the syntax, not the content. :)

***

> What the hell? Those templates are a mess!

I know. I'm still trying to figure out a way to best allow easy creation of templates. 

## CSS Syntax Highlighting Classes ##

Apply these classes to your elements and they will be syntax highlighted.
These are generated by Pygments.

- `c`  = comment; e.g. ` # this is a comment `
- `k`  = keyword; e.g. `for`, `if`, `class`
- `l`  = literal (string, numbers); e.g. `'string'`, `123`
- `m`  = numeric literal; e.g. `123` (like `l` but more specific)
- `s`  = string literal; e.g. `'string'` (like `l` but more specific)
- `nc` = class name declaration; e.g. `class ClassName()` (for ClassName)
- `nf` = function name declaration; e.g. `function funcName()` (for funcName)
- `vc` = class variable name; e.g. `class ClassName extends BaseClass` (for BaseClass)
- `n` = generic name; for everything else that might need highlighting


# Credits

- [wenbert][] for hosting and helping me deploy
- [mellort][] and [bboe][] for the [reddit\_api][api] Python library and for putting up with my incessant questioning
- Chloyd for being my guinea pig
- the Reddit community
- Alexander Sitte for the SQL templates

[wenbert]: http://blog.ekini.net
[mellort]: https://github.com/mellort/
[bboe]: https://github.com/bboe

