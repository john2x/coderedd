import unicodedata
import re

from django import template
from django.template.defaultfilters import stringfilter

register = template.Library()

@register.inclusion_tag('python/comments_tag.html')
def render_comments_python(comments):
    return {'comments': comments}

@register.inclusion_tag('java/comments_tag.html')
def render_comments_java(comments):
    return {'comments': comments}

@register.inclusion_tag('sql/comments_tag.html')
def render_comments_sql(comments):
    return {'comments': comments}

@register.filter
@stringfilter
def pythonstyle(value):
    special_chars = '!@#$%^&*()+-={}|[]\;\'\n:",./<>?'
    text = unicodedata.normalize('NFKD', value).encode('ascii', 'ignore')
    cleaned_text = text.translate(None, special_chars)
    cleaned_text = cleaned_text.replace(' ', '_').replace('-', '_').lower()
    return cleaned_text

def to_upper(matchobj):
    return matchobj.group(2).upper()

@register.filter
@stringfilter
def titlecase(value):
    return "".join(word.title() for word in value.split('_'))

@register.filter
@stringfilter
def camelcase(value):
    s = "".join(word.title() for word in value.split('_'))
    s1 = list(s)
    s1[0] = s1[0].lower()
    return "".join(c for c in s1)

@register.filter
@stringfilter
def get_submission_slug(permalink):
    ''' Given a reddit post permalink, get its slug '''
    return permalink.split('/')[7]

@register.filter
def timeago(time):
    ''' 
    Convert time to time ago 
    http://stackoverflow.com/questions/1551382/python-user-friendly-time-format
    '''
    from datetime import datetime
    now = datetime.now()
    time = int(time)
    if type(time) is int:
        diff = now - datetime.fromtimestamp(time)
    elif isinstance(time,datetime):
        diff = now - time 
    elif not time:
        diff = now - now
    second_diff = diff.seconds
    day_diff = diff.days

    if day_diff < 0:
        return str(datetime.fromtimestamp(time))

    if day_diff == 0:
        if second_diff < 10:
            return "just now"
        if second_diff < 60:
            return str(second_diff) + " seconds ago"
        if second_diff < 120:
            return  "a minute ago"
        if second_diff < 3600:
            return str( second_diff / 60 ) + " minutes ago"
        if second_diff < 7200:
            return "an hour ago"
        if second_diff < 86400:
            return str( second_diff / 3600 ) + " hours ago"
    if day_diff == 1:
        return "Yesterday"
    if day_diff < 7:
        return str(day_diff) + " days ago"
    if day_diff < 31:
        return str(day_diff/7) + " weeks ago"
    if day_diff < 365:
        return str(day_diff/30) + " months ago"
    return str(day_diff/365) + " years ago"

@register.filter
def get_type(value):
    return str(type(value))

@register.filter
@stringfilter
def is_image(value):
    return value.split('.')[-1] in ('jpg', 'jpeg', 'png', 'gif', 'bmp')

@register.filter
@stringfilter
def is_nsfw(value):
    return 'NSFW' in value.upper()

@register.filter
def subtract(value, operand):
    return int(value) - int(operand)

@register.filter
def as_string(value):
    return str(value)

