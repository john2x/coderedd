from django.conf import settings
from django.core.urlresolvers import reverse
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.views.decorators.cache import cache_control

import reddit
from reddit.errors import *

r = reddit.Reddit(user_agent='coderedd')

def init_session(request):
    if 'syntax' not in request.session:
        request.session['syntax'] = 'python'
    elif 'syntax' in request.GET:
        request.session['syntax'] = request.GET['syntax']

    if 'colorscheme' not in request.session:
        request.session['colorscheme'] = 'default'
    elif 'colorscheme' in request.GET:
        request.session['colorscheme'] = request.GET['colorscheme']

    if 'num_stories' not in request.session:
        request.session['num_stories'] = settings.DEFAULT_NUM_STORIES
    if 'count' not in request.session:
        request.session['count'] = 0
    if 'syntax' in request.GET:
        request.session['syntax'] = request.GET['syntax']
    if 'preview' not in request.session:
        request.session['preview'] = 'yes'

@cache_control(private=True, must_revalidate=True)
def index(request):
    init_session(request)
    limit = int(request.session['num_stories'])
    stories = []
    user = None
    my_reddits = None
    if 'count' in request.GET:
        count = int(request.GET['count'])
    else:
        count = limit

    if request.GET.get('after'):
        stories = list(r.get_content('http://www.reddit.com/', limit=limit,
                                     url_data={'count': str(count),
                                               'after': request.GET['after']}
                                    )
                      )
        before = stories[0].id
        after = stories[-1].id
        count += limit
    elif request.GET.get('before'):
        stories = list(r.get_content('http://www.reddit.com/', limit=limit,
                                     url_data={'count': str(count),
                                               'before': request.GET['before']}
                                    )
                      )
        before = stories[0].id
        after = stories[-1].id
        count -= 1
    else:
        stories = list(r.get_front_page(limit=limit))
        before = stories[0].id
        after = stories[-1].id

    if r.user:
        user = r.user
        my_reddits = list(user.my_reddits())
    return render_to_response('%s/index.html' % request.session['syntax'],
                       {'stories':stories, 'count':count, 'before':before,
                        'after':after, 'countbefore':(count-limit+1),
                        'user':user, 'my_reddits':my_reddits},
                       context_instance=RequestContext(request))

@cache_control(private=True, must_revalidate=True)
def subreddit(request, subreddit):
    init_session(request)
    limit = int(request.session['num_stories'])
    stories = []
    user = None
    my_reddits = None
    count = limit
    sr = r.get_subreddit(subreddit)
    url = 'http://www.reddit.com/r/%s/' % str(subreddit)
    if request.GET.get('after'):
        stories = list(r.get_content(url, limit=limit,
                                     url_data={'count': str(limit),
                                               'after': request.GET['after']}
                                    )
                      )
        before = stories[0].id
        after = stories[-1].id
        count += limit
    elif request.GET.get('before'):
        stories = list(r.get_content(url, limit=limit,
                                     url_data={'count': str(limit),
                                               'before': request.GET['before']}
                                    )
                      )
        before = stories[0].id
        after = stories[-1].id
        count -= 1
    else:
        stories = list(r.get_subreddit(subreddit).get_hot())
        before = stories[0].id
        after = stories[-1].id

    if r.user:
        user = r.user
        my_reddits = list(user.my_reddits())
    return render_to_response('%s/subreddit.html' % request.session['syntax'],
                       {'stories':stories, 'count':count, 'before':before,
                        'after':after, 'countbefore':(count-limit+1),
                        'user':user, 'my_reddits':my_reddits, 'subreddit':sr},
                       context_instance=RequestContext(request))

@cache_control(private=True, must_revalidate=True)
def comments(request, subreddit, story_id, story_slug):
    init_session(request)
    story = r.get_submission(submission_id=story_id)
    user = None
    my_reddits = None
    if r.user:
        user = r.user
        my_reddits = list(user.my_reddits())
    return render_to_response('%s/comments.html' % request.session['syntax'],
                              {'story':story, 'comments':story.comments_flat,
                               'user': user, 'my_reddits':my_reddits},
                              context_instance=RequestContext(request))

@cache_control(private=True, must_revalidate=True)
def login(request):
    error = None
    if request.method == 'POST':
        try:
            r.login(username=request.POST['username'],
                    password=request.POST['password'])
        except InvalidUserPass, e:
            error = 'Incorrect username or password. '

        if not error:
            request.session['username'] = request.POST['username']
            return HttpResponseRedirect(reverse('index'))

    return render_to_response('login.html', {'error':error},
                              context_instance=RequestContext(request))

@cache_control(private=True, must_revalidate=True)
def logout(request):
    try:
        r.logout()
    except NotLoggedIn, e:
        pass
    except LoginRequired, e:
        raise Http404

    request.session.pop('username')

    return HttpResponseRedirect(reverse('index'))

@cache_control(private=True, must_revalidate=True)
def update_options(request):
    syntax = request.GET.get('syntax')
    colorscheme = request.GET.get('colorscheme')
    next_url = str(request.GET.get('next'))
    preview  = str(request.GET.get('preview'))

    request.session['syntax'] = syntax
    request.session['colorscheme'] = colorscheme
    request.session['preview'] = preview
    return HttpResponseRedirect(next_url)

