from django.conf.urls import patterns, include, url
from django.views.generic.simple import direct_to_template

from views import *
# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns(
    '',
    # Examples:
    # url(r'^$', 'coderedd.views.home', name='home'),
    # url(r'^coderedd/', include('coderedd.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
    url(r'^$', index, name='index'),
    url(r'^about/$', direct_to_template, {'template': 'about.html'}, name='about'),
    url(r'^news/$', direct_to_template, {'template': 'news.html'}, name='news'),
    url(r'^login/$', login, name='login'),
    url(r'^logout/$', logout, name='logout'),
    url(r'^update_options/$', update_options, name='update_options'),
    url(r'^r/(?P<subreddit>[\w.-_]+)/comments/(?P<story_id>[\w_]+)/(?P<story_slug>[\w_]+)/$', comments, name='comments'),
    url(r'^r/(?P<subreddit>[\w.-_]+)/$', subreddit, name='subreddit'),

)
