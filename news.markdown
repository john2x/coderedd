[coderedd.net][] news
=====================

* * *

Welcome to coderedd.net!
------------------------
**2012-04-10**

coderedd.net is a custom UI for reddit.com inspired by code editors. It makes browsing reddit look like you're coding! It is available in various programming languages (syntax) and with different color schemes to match your preferred development environment.

It was originally called r.doqdoq and ran on Flask, but has been rewritten to run on Django.

### Why the rewrite?

I needed (and wanted) to transfer to a different hosting provider and went with [WebFaction][webfaction_referral](affiliate link). Since WebFaction doesn't have (default) support for Flask, I had to rewrite the site to run on Django. 

### What's new?

Not much, yet. 

- new SQL syntax.
- now using the latest version of [reddit\_api][]

I promise to add more "soon". :-P

### Bugs!

That said, I didn't get to test it as much. My old server went offline yesterday, so I had to deploy this one a bit prematurely. 

Send your bug reports to [john2x@coderedd.net](mailto:john2x@coderedd.net).

Enjoy!

 -- [john2x][]

[webfaction_referral]: http://www.webfaction.com?affiliate=john2x
[reddit\_api]: https://github.com/mellort/reddit_api

* * * 

[john2x]: http://john2x.com
[coderedd.net]: http://coderedd.net
