from django.conf import settings

def options(request):
    return {
            'SYNTAXES': settings.SYNTAXES,
            'COLORSCHEMES': settings.COLORSCHEMES,
            'SITENAME': settings.SITENAME
           }

